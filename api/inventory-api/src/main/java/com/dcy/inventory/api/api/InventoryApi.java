package com.dcy.inventory.api.api;

import com.dcy.inventory.api.dto.InventoryDTO;
import org.dromara.hmily.annotation.Hmily;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2021/11/16 14:46
 */
public interface InventoryApi {

    /**
     * 扣减库存操作
     * 这一个tcc接口
     *
     * @param inventoryDTO 库存DTO对象
     * @return true boolean
     */
    @Hmily
    Boolean decrease(InventoryDTO inventoryDTO);

    /**
     * mock扣减库存异常
     *
     * @param inventoryDTO dto对象
     * @return String string
     */
    @Hmily
    String mockWithTryException(InventoryDTO inventoryDTO);

    /**
     * mock扣减库存超时
     *
     * @param inventoryDTO dto对象
     * @return String boolean
     */
    @Hmily
    Boolean mockWithTryTimeout(InventoryDTO inventoryDTO);
}
