package com.dcy.account.api.api;

import com.dcy.account.api.dto.AccountDTO;
import org.dromara.hmily.annotation.Hmily;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2021/11/16 14:44
 */
public interface AccountApi {


    /**
     * 扣款支付
     *
     * @param accountDTO
     * @return
     */
    @Hmily
    boolean payment(AccountDTO accountDTO);


    /**
     * Mock try payment exception.
     *
     * @param accountDTO the account dto
     */
    @Hmily
    boolean mockTryPaymentException(AccountDTO accountDTO);
}
