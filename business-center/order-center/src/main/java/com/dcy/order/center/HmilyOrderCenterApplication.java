package com.dcy.order.center;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author dcy
 */
@SpringBootApplication(scanBasePackages = "com.dcy")
@MapperScan(basePackages = {"com.dcy.*.center.mapper"})
public class HmilyOrderCenterApplication {

    public static void main(String[] args) {
        SpringApplication.run(HmilyOrderCenterApplication.class, args);
    }

}
