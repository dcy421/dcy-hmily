package com.dcy.order.center.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dcy.order.api.model.Order;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author dcy
 * @since 2021-11-16
 */
public interface OrderMapper extends BaseMapper<Order> {

}
