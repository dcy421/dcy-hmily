package com.dcy.order.center.service;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.dcy.order.api.enums.OrderStatusEnum;
import com.dcy.order.api.model.Order;
import org.dromara.hmily.common.utils.IdWorkerUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.Date;

@SpringBootTest
class OrderServiceTest {

    @Autowired
    private OrderService orderService;


    @Test
    void orderPay() {
        orderService.orderPay(1, new BigDecimal(100));
    }

    @Test
    void addOrder() {
        Order order = new Order();
        order.setCreateTime(new Date());
        order.setNumber(IdWorker.getIdStr());
        //demo中的表里只有商品id为1的数据
        order.setProductId("1");
        order.setStatus(OrderStatusEnum.NOT_PAY.getCode());
        order.setTotalAmount(new BigDecimal(100));
        order.setCount(1);
        //demo中 表里面存的用户id为10000
        order.setUserId("10000");
        orderService.save(order);
    }
}