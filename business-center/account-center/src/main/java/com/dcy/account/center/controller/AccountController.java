package com.dcy.account.center.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2021/11/16 14:19
 */
@RestController
public class AccountController {

    @GetMapping("/index")
    public String index() {
        return "xxx";
    }

}
