package com.dcy.account.center;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author dcy
 */
@SpringBootApplication(scanBasePackages = "com.dcy")
@MapperScan(basePackages = {"com.dcy.*.center.mapper"})
public class HmilyAccountCenterApplication {

    public static void main(String[] args) {
        SpringApplication.run(HmilyAccountCenterApplication.class, args);
    }

}
