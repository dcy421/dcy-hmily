package com.dcy.account.center.service;

import com.dcy.inventory.api.api.InventoryApi;
import com.dcy.inventory.api.dto.InventoryDTO;
import org.apache.dubbo.config.annotation.DubboReference;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class AccountServiceTest {

    @Autowired
    private AccountService accountService;
    @DubboReference(version = "1.0.0", check = false, url = "dubbo://192.168.0.38:20882")
    private InventoryApi inventoryApi;

    @Test
    void listAll() {
        accountService.list().forEach(System.out::println);
    }

    @Test
    void inventoryList() {
        final InventoryDTO inventoryDTO = new InventoryDTO();
        inventoryDTO.setCount(123);
        inventoryApi.decrease(inventoryDTO);
    }
}