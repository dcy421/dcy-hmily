# hmily TCC框架学习

调用分布式事务，必须是接口，不可以是类

## 代码演示
### 定义接口
```java
public interface PaymentService {

    /**
     * 订单支付
     *
     * @param order 订单实体
     */
    void makePayment(Order order);
    
}
```

### 定义实现
```java
@Slf4j
@Service
public class PaymentServiceImpl implements PaymentService {
    /**
     * 订单支付
     *
     * @param order 订单实体
     */
    @Override
    @HmilyTCC(confirmMethod = "confirmOrderStatus", cancelMethod = "cancelOrderStatus")
    public void makePayment(Order order) {
        updateOrderStatus(order, OrderStatusEnum.PAYING);
        //扣除用户余额
        accountApi.payment(buildAccountDTO(order));
        //进入扣减库存操作
        inventoryApi.decrease(buildInventoryDTO(order));
    }

    public void confirmOrderStatus(Order order) {
        updateOrderStatus(order, OrderStatusEnum.PAY_SUCCESS);
        log.info("=========进行订单confirm操作完成================");
    }

    public void cancelOrderStatus(Order order) {
        updateOrderStatus(order, OrderStatusEnum.PAY_FAIL);
        log.info("=========进行订单cancel操作完成================");
    }
}

```

### 远程接口
```java
public interface AccountApi {


    /**
     * 扣款支付
     *
     * @param accountDTO
     * @return
     */
    @Hmily
    boolean payment(AccountDTO accountDTO);

}

```

实现类
```java
@Slf4j
@DubboService(version = "1.0.0", timeout = 3000, retries = 0, interfaceClass = AccountApi.class)
public class AccountService extends ServiceImpl<AccountMapper, Account> implements AccountApi {

    /**
     * The Confrim count.
     */
    private static AtomicInteger confrimCount = new AtomicInteger(0);

    @Override
    @HmilyTCC(confirmMethod = "confirm", cancelMethod = "cancel")
    public boolean payment(AccountDTO accountDTO) {
        int count = baseMapper.update(accountDTO);
        log.info("payment {} {}", count, accountDTO);
        if (count > 0) {
            return true;
        } else {
            throw new HmilyRuntimeException("账户扣减异常！");
        }
    }

    /**
     * Confirm boolean.
     *
     * @param accountDTO the account dto
     * @return the boolean
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean confirm(AccountDTO accountDTO) {
        log.info("============dubbo tcc 执行确认付款接口===============");
        baseMapper.confirm(accountDTO);
        final int i = confrimCount.incrementAndGet();
        log.info("调用了account confirm " + i + " 次");
        return Boolean.TRUE;
    }

    /**
     * Cancel boolean.
     *
     * @param accountDTO the account dto
     * @return the boolean
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean cancel(AccountDTO accountDTO) {
        log.info("============ dubbo tcc 执行取消付款接口===============");
        final Account account = baseMapper.findByUserId(accountDTO.getUserId());
        baseMapper.cancel(accountDTO);
        return Boolean.TRUE;
    }
}
```